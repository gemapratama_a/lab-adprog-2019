package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
  
    public ModelDuck() {
    	super();
    	setQuackBehavior(new MuteQuack());
    	setFlyBehavior(new FlyNoWay());
    }

    @Override
	public void display() {
		System.out.println("Displaying Model Duck");
	}
}
