package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class CurrentConditionsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float temperature;
    private float humidity;

    public CurrentConditionsDisplay(Observable observable) {
        this.observable = observable;
        this.observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Current conditions: " + temperature
                + "F degrees and " + humidity + "% humidity");
    }
 
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData observable = (WeatherData) o;
            this.temperature = observable.getTemperature();
            this.humidity = observable.getHumidity();
        }
    }
}
