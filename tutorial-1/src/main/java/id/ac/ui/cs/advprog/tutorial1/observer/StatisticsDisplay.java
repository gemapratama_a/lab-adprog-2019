package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;
    
    public StatisticsDisplay(Observable observable) {
        // TODO Complete me!
        this.observable = observable;
        this.observable.addObserver(this);
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.numReadings++;
        if (o instanceof WeatherData) {
            WeatherData observable = (WeatherData) o;
            float newTemperature = observable.getTemperature();

            tempSum += newTemperature;

            if (newTemperature >= maxTemp) {
                this.maxTemp = newTemperature;
            } else if (newTemperature <= minTemp) {
                this.minTemp = newTemperature;
            }
        }
        
    }
}