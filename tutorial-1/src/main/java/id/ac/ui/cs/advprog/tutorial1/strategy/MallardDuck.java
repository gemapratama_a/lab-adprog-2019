package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
   
	public MallardDuck() {
		super();
		setQuackBehavior(new Squeak());
		setFlyBehavior(new FlyWithWings());
	}

	@Override
	public void display() {
		System.out.println("Displaying Mallard Duck");
	}
}
