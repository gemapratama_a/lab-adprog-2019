import id.ac.ui.cs.advprog.tutorial1.observer.*;

/*References: 
 https://www.youtube.com/watch?v=_BpmfnqjgzQ
 http://www.newthinktank.com/2012/08/observer-design-pattern-tutorial/
 https://docs.oracle.com/javase/7/docs/api/java/util/Observable.html
 https://docs.oracle.com/javase/7/docs/api/java/util/Observer.html
 https://stackoverflow.com/questions/32535951/whats-the-significance-of-the-second-arguement-in-update-in-observer-interfac
 https://stackoverflow.com/questions/15724917/observer-pattern-in-java-observer-observes-different-unrelated-types-or-class
 https://stackoverflow.com/questions/13901339/java-observer-update-function
 https://examples.javacodegeeks.com/core-java/util/observer/java-util-observer-example/
 https://www.webucator.com/how-to/how-compile-packages-java.cfm
*/
public class WeatherStation {

    public static void main(String[] args) {
        WeatherData weatherData = new WeatherData(); 

        CurrentConditionsDisplay currentConditions = new CurrentConditionsDisplay(weatherData);
        StatisticsDisplay statisticsDisplay = new StatisticsDisplay(weatherData);
        ForecastDisplay forecastDisplay = new ForecastDisplay(weatherData);

        weatherData.addObserver(currentConditions);
        weatherData.addObserver(statisticsDisplay);
        weatherData.addObserver(forecastDisplay);
      
        weatherData.setMeasurements(80, 65, 30.4f);
        weatherData.setMeasurements(82, 70, 29.2f);
        weatherData.setMeasurements(78, 90, 29.2f);
		/*
        currentConditions.display();
        statisticsDisplay.display();
        forecastDisplay.display();
		*/
    }
}
