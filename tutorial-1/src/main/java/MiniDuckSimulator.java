import id.ac.ui.cs.advprog.tutorial1.strategy.*;
/** 
  References: 
  https://www.youtube.com/watch?v=v9ejT8FO-7I    
  https://www.youtube.com/watch?v=-NCgRD9-C6o
  http://www.newthinktank.com/2012/08/strategy-design-pattern-tutorial/
  */
public class MiniDuckSimulator {

    public static void main(String[] args) {
        
        Duck mallard = new MallardDuck();
        mallard.performQuack();
        mallard.performFly();

        Duck model = new ModelDuck();
        model.performFly();
        model.setFlyBehavior(new FlyRocketPowered());
        model.performFly();
    }
}
